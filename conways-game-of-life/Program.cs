﻿using System;
using System.Runtime.CompilerServices;

namespace Conways_Game_Of_Life
{
    class Program
    {
        static void Main(string[] args)
        {
            GameOfLife GameField = new GameOfLife(10,10);
            GameField.RandomizeBoard();
            ConsoleKeyInfo input;

            do
            {
                GameField.DrawBoard();
                GameField.TurnBoard();
                input = Console.ReadKey();
            } while (input.Key != ConsoleKey.Escape) ;
        }
    }
}
