﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Conways_Game_Of_Life
{
    class GameOfLife
    {
        private bool[,] board { get; set; }

        public GameOfLife(int yAxis, int xAxis)
        {
            board = new bool[yAxis, xAxis];
        }


        public void RandomizeBoard()
        {
            Random rnd = new Random();
            for (int yAxis = 0; yAxis < board.GetLength(0); yAxis++)
            {
                for (int xAxis = 0; xAxis < board.GetLength(1); xAxis++)
                {
                    int Random = rnd.Next(101);
                    if (Random <= 50) board[yAxis, xAxis] = true;
                }
            }
        }

        public void ClearBoard()
        {
            for (int yAxis = 0; yAxis < board.GetLength(0); yAxis++)
            {
                for (int xAxis = 0; xAxis < board.GetLength(1); xAxis++) board[yAxis, xAxis] = false;
            }
        }

        public void DrawBoard()
        {
            for (int yAxis = 0; yAxis < board.GetLength(0); yAxis++)
            {
                for (int xAxis = 0; xAxis < board.GetLength(1); xAxis++)
                {
                    if (board[yAxis, xAxis]) Console.Write('+');
                    else Console.Write('-');
                }
                Console.WriteLine();
            }
        }

        public void SetCellStates(List<Tuple<int, int>> Coordinates, bool IsLiving)
        {
                foreach (Tuple<int, int> Location in Coordinates)
                {
                    if(Location.Item1 + 1 <= board.GetLength(0) && Location.Item2 + 1 <= board.GetLength(1) && Location.Item1 - 1 >= 0 && Location.Item2 - 1 >= 0) board[Location.Item1, Location.Item2] = IsLiving;
                }
        }

        public int CountLivingNeighbors(int yAxis, int xAxis)
        {
            int LivingNeighbors = 0;

            if (yAxis - 1 >= 0 && board[yAxis - 1, xAxis]) LivingNeighbors++;

            if (yAxis + 1 <= board.GetLength(0) - 1 && board[yAxis + 1, xAxis]) LivingNeighbors++;

            if (yAxis - 1 >= 0 && xAxis - 1 >= 0 && board[yAxis - 1, xAxis - 1]) LivingNeighbors++;

            if (yAxis - 1 >= 0 && xAxis + 1 <= board.GetLength(1) - 1 && board[yAxis - 1, xAxis + 1]) LivingNeighbors++;

            if (xAxis - 1 >= 0 && board[yAxis, xAxis - 1]) LivingNeighbors++;

            if (xAxis + 1 <= board.GetLength(1) - 1 && board[yAxis, xAxis + 1]) LivingNeighbors++;

            if (yAxis + 1 <= board.GetLength(0) -1 && xAxis - 1 >= 0 && board[yAxis + 1, xAxis - 1]) LivingNeighbors++;

            if (yAxis + 1 <= board.GetLength(0) - 1 && xAxis + 1 <= board.GetLength(1) - 1 && board[yAxis + 1, xAxis + 1]) LivingNeighbors++;

            return LivingNeighbors;
        }

        public void TurnBoard()
        {
            for (int yAxis = 0; yAxis < board.GetLength(0); yAxis++)
            {
                for (int xAxis = 0; xAxis < board.GetLength(1); xAxis++)
                {
                    int livingNeighbors = CountLivingNeighbors(yAxis, xAxis);

                    if (board[yAxis, xAxis])
                    {
                        if (livingNeighbors == 2 || livingNeighbors == 3) board[yAxis, xAxis] = true;
                        else board[yAxis, xAxis] = false;
                    }
                    else
                    {
                        if (livingNeighbors == 3) board[yAxis, xAxis] = true;
                        else board[yAxis, xAxis] = false;
                    }
                }
            }
        }
    }
}
